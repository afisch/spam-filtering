###############################################################################
# feature_selection.py
# Authors: Adam Fisch and Max Shatkhin
#
# COS 424 Assignment 1
###############################################################################
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from email_process import read_bagofwords_dat
from email_classifier import feature_test, compare_classifiers, optimize_hyperparam
from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import math

# Selects the top num features from a feature vector space by using the chi2
# statistic. X is a 2D array of n_samples x m_features, y is 1D of n_labels.
def selectFeatures(XTrain, yTrain, XTest, percent):
    init_feat_size = XTrain.shape[1]
    num = int((percent / 100.0) * init_feat_size)
    selector = SelectKBest(chi2, k=num)
    XTrainNew = selector.fit_transform(XTrain, yTrain)
    XTestNew = selector.transform(XTest)
    print("Selected...")
    return (XTrainNew, XTestNew, num)

def printBestFeatures(XTrain, yTrain, vocabFileName, num):
    selector = SelectKBest(chi2, k=num)
    XTrainNew = selector.fit_transform(XTrain, yTrain)
    chosen = selector.get_support()
    vocab = []
    vocabFile = open(vocabFileName, "r")
    for line in vocabFile:
        vocab.append(line.rstrip())
    vocabFile.close()

    newVocab = []
    for i in range(len(vocab)):
        if (chosen[i]):
            newVocab.append(vocab[i])
    print(newVocab)
    return

# This will print out the latex table formatting for data for both standard
# bow and augmented bow models. Dimensions of data should be num_models x 2.
def latexResults(errors, precisions, recalls, f1s, run_times, models):
    for i in range(len(models)):
        print(models[i,0] + ' & ' + errors[i,0] + ' & '+precisions[i,0] + ' & '
              + recalls[i,0] + ' & ' + f1s[i,0] + ' & ' + run_times[i,0]
              + errors[i,1] + ' & ' + precisions[i,1] + ' & ' + recalls[i,0]
              + ' & ' + f1s[i,0] + ' & ' + run_times[i,0] +r"\\")

def readData(featureFileName, labelFileName):
    featuresVector = read_bagofwords_dat(featureFileName, numofemails = 45000)
    labelsFile = open(labelFileName, "r")
    labelsVector = []
    for line in labelsFile:
        labelsVector.append(line.rstrip())
    labelsFile.close()

    #split into train and test sets. Use 90% and 10%.
    labelsTrain = labelsVector[0:int(.9*len(labelsVector))]
    featuresTrain = featuresVector[0:int(.9*len(featuresVector))]
    labelsTest = labelsVector[int(.9*len(labelsVector)):]
    featuresTest = featuresVector[int(.9*len(featuresVector)):]
    return (featuresTest, labelsTest, featuresTrain, labelsTrain)

def readDataSplit(trainFeatures, trainLabels, testFeatures, testLabels):
    XTrain = read_bagofwords_dat(trainFeatures, numofemails = 45000)
    trainLabelsFile = open(trainLabels, "r")
    yTrain = []
    for line in trainLabelsFile:
        yTrain.append(line.rstrip())
    trainLabelsFile.close()

    XTest = read_bagofwords_dat(testFeatures, numofemails = 5000)
    testLabelsFile = open(testLabels, "r")
    yTest = []
    for line in testLabelsFile:
        yTest.append(line.rstrip())
    testLabelsFile.close()

    return (XTest, yTest, XTrain, yTrain)

def main():

    #debug reduces size of training/test set to make runtime shorter
    debug = 0

    # Read in the saved data
    labelFileName = "../trec07p_data/Bigrams/train_emails_classes_200.txt"
    vocabFileName = "../trec07p_data/Bigrams/train_emails_vocab_200.txt"
    featureFileName = "../trec07p_data/Bigrams/train_emails_bag_of_words_200.dat"
    testLabels = "../trec07p_data/Bigrams/test_emails_classes_0.txt"
    testFeatures = "../trec07p_data/Bigrams/test_emails_bag_of_words_0.dat"
    (XTest, yTest, XTrain, yTrain) = readDataSplit(featureFileName, labelFileName, testFeatures, testLabels)

    print("Original data dimensions:")
    print("XTest: %s yTest: %s XTrain: %s yTrain: %s"
          % (XTest.shape, len(yTest), XTrain.shape, len(yTrain)))

    #reduce the size of the training/test set
    if (debug == 1):
        XTest_d = np.vstack((XTest[0:int(.1*len(XTest)),:],
                             XTest[int(.9*len(XTest)):,:]))
        yTest_d = (yTest[0:int(.1*len(yTest))]
                 + yTest[int(.9*len(yTest)):])
        XTrain_d = np.vstack((XTrain[0:int(.1*len(XTrain)),:],
                              XTrain[int(.9*len(XTrain)):,:]))
        yTrain_d = (yTrain[0:int(.1*len(yTrain))]
                  + yTrain[int(.9*len(yTrain)):])

        (XTest, yTest, XTrain, yTrain) = (XTest_d, yTest_d, XTrain_d, yTrain_d)

    print("New data dimensions for debugging:")
    print("XTest: %s yTest: %s XTrain: %s yTrain: %s"
          % (XTest.shape, len(yTest), XTrain.shape, len(yTrain)))

    #printBestFeatures(XTrain, yTrain, vocabFileName, 1000)

    dataSize = XTest.shape
    feature_percent = range(1,100,5)
    num_tests = len(feature_percent)
    errors = [0] * num_tests
    recalls = [0] * num_tests
    precisions = [0] * num_tests
    run_times = [0] * num_tests
    num_features = []

    print("Varying the number of selected features...")
    print("Initial feature size  = %s" % dataSize[1])

    #call function to get optimal hyperparameters (takes forever)
    #classifiers_with_hp = ['knn','ada','dt']
    #optimal_hp = optimize_hyperparam(classifiers_with_hp, XTrain,yTrain)
    optimal_hp = {'knn':5,'ada':50,'dt':20}


    # FEATURE VARIATION
    for i,f_p in enumerate(feature_percent):
        print ("feature percent: %s%%" % f_p)
        (XTrainNew, XTestNew, num) = selectFeatures(XTrain, yTrain, XTest, f_p)
        (errors[i], recalls[i], precisions[i], run_times[i], classifiers) = \
            feature_test(XTrainNew, yTrain, XTestNew, yTest, optimal_hp)
        num_features.append(num)
    print("Finished.")

    # turn into matrix format
    errors = np.array(errors)
    recalls = np.array(recalls)
    precisions = np.array(precisions)
    run_times = np.array(run_times)
    
    # plot
    colors = ['b','g','r','c','m','y','k']
    classifiers = ['NB-Gaussian', 'NB-Multinomial', 'Decision Tree',
                    'SVM','adaBoost','kNN']
    figure()
    for c in range(errors.shape[1]):
        plot(num_features, errors[:,c], colors[c] +'-',label=classifiers[c])

    ymin, ymax = ylim()
    ylim([0, ymax+1])
    xlabel('Number of Features')
    ylabel('Generalization Error (%)')
    title('Classifier Performance vs. Feature Vector Size')
    grid()
    legend(loc = 'best', prop = {'size':10})



    # CLASSIFIER COMPARISON
    print ('Comparing Classifiers...')
    (XTrainNew, XTestNew, num) = selectFeatures(XTrain, yTrain, XTest, 10)  #reduce size of feature set
    (fpr, tpr, auc, f1, precision, recall, p_score, r_score, error, times) = compare_classifiers(XTrainNew, yTrain, XTestNew, yTest, optimal_hp)

    print ('Finished.')
    print ('Plotting Metrics')

    print ('precision = %s ' % p_score)
    print ('recall = %s ' % r_score) 
    print ('error = %s ' % error)
    print ('time = %s ' % times)
    print ('f1 = %s ' % f1)
    print ('auc = %s ' % auc)

    #plot ROC
    figure()
    for c in range(len(fpr)):
        this_fpr, this_tpr = fpr[c], tpr[c]
        plot(this_fpr, this_tpr, colors[c] + '-', label = classifiers[c])

    plot([0,1], [0,1], '--', color = (.6, .6, .6), label = 'Luck')
    xlabel('False Positive Rate')
    ylabel('True Positive Rate')
    title('Reciever Operating Characteristic')
    legend(loc = 'best', prop={'size':10})
    show()

    #plot precision-recall
    figure()
    for c in range(len(fpr)):
        this_precision, this_recall = precision[c], recall[c]
        plot(this_recall, this_precision, colors[c] + '-', label=classifiers[c])
    xlabel('Recall')
    ylabel('Precision')
    title('Precision-Recall')
    legend(loc = 'best', prop={'size':10})
    show()

    #plot f1 (bar graph)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    idx = range(len(classifiers))
    width = .5
    rects = ax.bar(idx,f1,width)
    ax.set_xlim(-width,len(idx)+width)
    ax.set_ylim(.8*max(f1), 1.2*max(f1))
    ax.set_ylabel('F1 scores')
    ax.set_title('Classifiers')
    xTickMarks = classifiers
    ax.set_xticks(idx)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=45, fontsize=10)
    plt.show()

    #plot auc (bar graph)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    idx = range(len(classifiers))
    width = .5
    rects = ax.bar(idx,auc,width)
    ax.set_xlim(-width,len(idx)+width)
    ax.set_ylim(.8*max(auc), 1.2*max(auc))
    ax.set_ylabel('AUC scores')
    ax.set_title('Classifiers')
    xTickMarks = classifiers
    ax.set_xticks(idx)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=45, fontsize=10)
    plt.show()

if __name__ == "__main__":
   main()
