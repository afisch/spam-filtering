\documentclass{article} % For LaTeX2e
\usepackage{cos424,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{multicol}
\usepackage{enumitem}
\usepackage{array}


\bibliographystyle{plos2009}


\title{Email Classification}

\nipsfinalcopy

\author{
Adam Fisch\\
Department of Mechanical Engineering\\
Princeton University\\
\texttt{afisch@princeton.edu} \\
\And
Max Shatkhin \\
Department of Electrical Engineering \\
Princeton University\\
\texttt{shatkhin@princeton.edul} \\
}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\begin{document}
 
\maketitle

\begin{abstract}


\end{abstract}
\section{Introduction}
	
	Email makes sending messages between recipients cheap, fast, and easy. Unfortunately, not all received messages are genuine or wanted. Email allows for efficient, mass distribution of unsolicited junk mail such as product advertisements, promotions, fraud schemes, or worse, malware \cite{cormack}. This type of mail is commonly referred to as spam. As a counter measure, many email filtering methods have been developed. These filters range from direct knowledge engineering strategies, such as blacklisting of known spammer IP addresses, to machine learning techniques, in which classifier algorithms are trained to automatically discriminate between spam and legitimate mail \cite{awad}.
	
	In this report we explore the latter approach, and evaluate the performance of several different classification methods. Developing a good classifier depends on many factors - starting with how an email is represented and ending with fine tuning of the specific parameters used in the learning model. We look at the influence of the selected email feature space on general performance, as well as the improvement of learning in response to increased training data. Finally, we compare the performance of the different classifiers relative to each other.  

\section{Email Dataset and Representation}

	The TREC 2007 spam data set is used as the source of labeled spam and not spam emails \cite{trec07}. It consists of $50,000$ total emails - evenly distributed between spam and not spam. 10\% of the corpus is held out of training to be used as the validation set. The email files are raw text files containing both the content and header information. 
	
	The dataset is converted into a numerical feature vector format using a bag-of-words (BOW) model. The BOW splits the emails into tokens (``words''), and compiles a vocabulary of all the words seen in training. Each email is represented by a vector of counts of how many times each word appears in its text. Words that are too rare or frequent to be meaningful are removed, and attempts are made at reducing the proliferation of different forms of the same words. This is done with the Python NLTK convert to lower case, stop word removal, stemming, and lemmatization methods \cite{NLTK}.

	The BOW model operates on the assumption that spam emails use a distinctly different vocabulary from normal emails. Spam mail typically contains more sell words, product names, html tags from embedded content, etc. However, the BOW creates very highly dimensioned feature vectors, despite the mentioned attempts to reduce the vocab. This creates problems for some types of classifiers. Additionally, context from word ordering is lost, and with that the semantics. Two emails with the same words will be indistinguishable. 
	
	To capture lost meaning, we experimented with augmenting the BOW model with word bigrams. For instance, $``click\ here"$ would be added to the set $\{click, here\}$. To reduce dimensionality of the feature space, which is even larger with the bigrams, we considered the effect of feature selection using $\chi^2$ analysis. The $\chi^2$ test is used to test the independence of a feature $x$ and the class label $c$ for a given dataset $\mathbb{D}$ \cite{nlp}:

\begin{equation}
\chi^2(\mathbb{D}, x, c) = \sum_{e_x \in \{0,1\}} \sum_{e_c \in \{0,1\}} \frac{(N_{e_xe_c} - E_{e_xe_c})^2}{E_{e_xe_c}}
\label{chi2}
\end{equation}

	N is the observed frequency, E is the expected frequency if $x$ and $c$ are independent, and $e_v = \{0,1\}$ denotes whether the random variable $v$ is true or not. Higher $\chi^2$ values indicate that a word is more correlated with a specific class, and more useful as a feature. Using SciKi-Learn \cite{scikit}, we look at how performance varies with incrementally choosing 1$\Rightarrow$100\% of the features, ranked by importance. 
	
\section{Classification Methods}
	
\subsection{Algorithms}
	Existing machine learning algorithms cover a diverse spectrum of approaches. They range from parametric and non-parametric models, from very simple to quite complicated hypothesis spaces, and from runtimes of seconds to days. Parametric classifiers summarize data with a fixed set of parameters. These models are usually fast and resistant to overfitting. In contrast, nonparametric models use memory-based learning and are not specified by a bounded set of parameters. With large datasets, these types of classifiers can do very well, and create complex decision functions \cite{AIMA}. 
	
	To sample and compare performance applied to spam filtering across the various techniques we constructed the following five, distinct classifiers that cover a large range of existing approaches to machine learning. All approaches were built using the SciKit-Learn library \cite{scikit}. 

\begin{enumerate}%[leftmargin=*]
\item \emph{Naive Bayes classifier} (NB): 
	Naive Bayes is a simple approach that assumes that the features are conditionally independent given the class label \cite{MLAPA}. Given the one-dimensional feature-class conditional densities $\boldsymbol{\theta}$, the class-conditional density is simplified to:

\begin{equation}
p(\boldsymbol{x}|y=c, \boldsymbol{\theta}) = \prod_{j=1}^{D}p(x_j|y=c, \boldsymbol{\theta}_{jc})
\end{equation}
	NB classifiers are fast, simple, and resistant to overfitting. However, much of their performance is dependent on the accuracy of the underlying probability distribution estimations $\boldsymbol{\theta}$, as well as the veracity of the independence assumption. For $\boldsymbol{\theta}$, we tried both gaussian (GNB) and multinomial (MNB) models.

\item \emph{Decision Tree} (DT):
	Decision trees operate by performing a sequence of tests on the feature values. For a variety of problems DTs can give very effective, concise results \cite{AIMA}. However, such a model is not guaranteed to be found. For $n$ boolean features, there are $2^{2n}$ possible DTs to choose from. This issue is compounded when dealing with non-binary features in the BOW model. Consequently, DTs can be subject to overfitting. For our classifier, we use Gini impurity scores to help pick the ordering of feature tests when building the tree. 
	
\item \emph{Support Vector Machine} (SVM):
	SVMs are a popular, non-parametric approach with a good ability to represent complex functions \cite{MLAPA}. SVMs retain the training example vectors and use optimization techniques to find a separating hyperplane for the data. SVMs can also use special kernels to transform data to higher dimensions, in which such separating hyperplanes might be found. Since our BOW feature space is already highly dimensioned, we used a SVM with a linear kernel.
	
\item \emph{$K$-Nearest Neighbors} (KNN):
KNN is a simple, data-driven, non-parametric classifier. Given an input feature vector, this algorithm looks in the training set for the $K$ closest examples. With a good distance metric and large training datasets, KNNs can work well. However, with high dimensional inputs KNN classifiers can suffer from the ``curse of dimensionality'' \cite{MLAPA}. In high dimensional space, the $K$ nearest neighbors of an input vector might still be very dissimilar. We experiment with the value of $K$ in our model.


\item \emph{AdaBoost} (AB):
AdaBoost is an example of ensemble learning, where as opposed to a single classifier, the hypothesis is formed from a collection of classifiers. AdaBoost minimizes the error of the overall classifier by using many, combined, weighted weak learners. AdaBoost is very effective for a large variety of problems \cite{shapire}.  

\end{enumerate}

\subsection{Evaluation Metrics}

	For classification problems, a natural, useful metric is the generalization error estimate from the misclassification rate on the validation set \cite{MLAPA}:
	
\begin{equation}
err(f, \mathbb{D}) = \frac{1}{N}\sum_{i=1}^{N}\mathbb{I}(f(\boldsymbol(x)_i \ne y_i)
\end{equation}

	In addition to mean error, we examine the classification results more closely in terms of their ratios of numbers of false positives (FP), true positives (TP), false negatives (FN), and true negatives (TN). We compare the classification outputs using precision (Pr), recall (Re), and F1 scores, defined below. We also look at ROC curves and computational requirements with regards to elapsed wall-clock time.	

\begin{multicols}{3}
 \begin{equation}
    Pr =  \frac {TP}{TP+FP}
 \end{equation}\break
 \begin{equation}
    Re = \frac{TP}{TP+FN}
 \end{equation}\break
  \begin{equation}
    F1 = \frac{2PrRe}{Pr+Re}
 \end{equation}
\end{multicols}
	
\section{Results}
\label{results}

\begin{table}[htbp]
\small
\setlength{\tabcolsep}{6pt}
\setlength\extrarowheight{3pt}
   \centering
   \begin{tabular}{@{}|c|c|c|c|c|c|c|c|c|c|c|@{}} % Column formatting, @{} suppresses leading/trailing space 
   \hline
    &  \multicolumn{5}{c|}{BOW} &
  \multicolumn{5}{c|}{BOW + Bigrams and Feature Selection}  \\
  \cline{2-11}
   Classifier & Error & Prec & Recall & $F_1$ & Time (s)& Error & Prec & Recall & $F_1$ & Time (s) \\ \hline\hline
      KNN   &  0.77 & 0.34 & 0.45 & 1480.4 & 0.74 & 0.40 & 0.54 & 272.1&0&0\\
        LR   &  0.81 & 0.89 & 0.85 & 23.7& 0.76 & 0.91 & 0.82 & 14.8&0&0 \\
  \hline
  \end{tabular}
  \caption{{\bf Results from ten classifiers on 20 Newsgroups data.} For each classifier, we report precision, recall,  $F_1$-scores, and wall clock time in seconds for the one-versus-rest classification task with 10-fold cross validation.}
   \label{tab:classifiers}
\end{table}

\section{Discussion and Conclusion}
\bibliography{ref}

\end{document}








