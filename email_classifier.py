
###############################################################################
# email_classifier.py
# Authors: Adam Fisch and Max Shatkhin
#
# COS 424 Assignment 1
###############################################################################
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import *
from sklearn.preprocessing import LabelEncoder
from sklearn import grid_search
import numpy as np
import time


def feature_test(XTrain, yTrain, XTest, yTest, optimal_hp):

    #first call to time.clock
    ref = time.time()

    #get prediction vectors for all classifiers
    predictions = []
    timing = []
    classifiers = []

    # NB-Gaussian
    start = time.time()
    predictions.append(naive_bayes_g(XTrain, yTrain, XTest)[0])
    elapsed = time.time() - start
    classifiers.append('NBG')
    timing.append(elapsed)

    # NB - Multi
    start = time.time()
    predictions.append(naive_bayes_m(XTrain, yTrain, XTest)[0])
    elapsed = time.time() - start
    classifiers.append('NBM')
    timing.append(elapsed)

    # DT
    start = time.time()
    predictions.append(decision_tree(XTrain, yTrain, XTest, optimal_hp['dt'])[0])
    elapsed = time.time() - start
    classifiers.append('DT')
    timing.append(elapsed)

    # SVM
    start = time.time()
    predictions.append(support_vector(XTrain, yTrain, XTest)[0])
    elapsed = time.time() - start
    classifiers.append('SVM')
    timing.append(elapsed)

    # AdaBoost
    start = time.time()
    predictions.append(ada_boost(XTrain, yTrain, XTest, optimal_hp['ada'])[0])
    elapsed = time.time() - start
    classifiers.append('AB')
    timing.append(elapsed)

    # kNN
    start = time.time()
    predictions.append(nearest_neighbors(XTrain, yTrain, XTest, optimal_hp['knn'])[0])
    elapsed = time.time() - start
    classifiers.append('KNN')
    timing.append(elapsed)
    ref = elapsed

    # performance metrics
    error =[]
    recall = []
    precision = []
    for i in range(len(predictions)):
        if (len(predictions[i]) == 0):
            continue
        error.append(find_error(predictions[i],yTest))
        recall.append(recall_score(yTest, predictions[i], pos_label='Spam'))
        precision.append(precision_score(yTest, predictions[i], pos_label='Spam'))

    # returns vectors of performance metrics where length of
    # vector is number of classifiers
    return (error, recall, precision, timing, classifiers)



def naive_bayes_g(XTrain, yTrain, XTest):
    gnb = GaussianNB()
    gnb.fit(XTrain, yTrain)
    scores = gnb.predict_proba(XTest)
    labels = gnb.predict(XTest)

    return (labels, scores)

def naive_bayes_m(XTrain, yTrain, XTest):
    mnb = MultinomialNB()
    mnb.fit(XTrain, yTrain)
    scores = mnb.predict_proba(XTest)
    labels = mnb.predict(XTest)

    return (labels, scores)

def decision_tree(XTrain, yTrain, XTest, depth):
    dt = DecisionTreeClassifier(criterion="gini",splitter="best",max_depth=depth)
    dt.fit(XTrain, yTrain)
    scores = dt.predict_proba(XTest)
    labels = dt.predict(XTest)

    return (labels, scores)

def support_vector(XTrain, yTrain, XTest):
    svm = SVC(kernel='linear',probability=True)
    svm.fit(XTrain, yTrain)
    scores = svm.predict_proba(XTest)
    labels = svm.predict(XTest)

    return (labels, scores)

def ada_boost(XTrain, yTrain, XTest, estimators):
    ada = AdaBoostClassifier(n_estimators = estimators) #Default base estimator is DT
    ada.fit(XTrain,yTrain)
    scores = ada.predict_proba(XTest)
    labels = ada.predict(XTest)

    return (labels, scores)

def nearest_neighbors(XTrain, yTrain, XTest, neighbors):
    knn = KNeighborsClassifier(n_neighbors = neighbors)
    knn.fit(XTrain,yTrain)
    scores = knn.predict_proba(XTest)
    labels = knn.predict(XTest)
    
    return (labels, scores)

###############################################################################
# Parameter tuning and performance code
###############################################################################

def optimize_hyperparam(classifiers, XTrain, yTrain):

    best_params = {}
    for classifier in classifiers:
        if (classifier == 'knn'):
            knn = KNeighborsClassifier()
            parameters = {'n_neighbors':range(1,10,3)}
            param_tuner = grid_search.GridSearchCV(knn,parameters)
            param_tuner.fit(XTrain, yTrain)
            best_params[classifier] = (param_tuner.best_params_['n_neighbors'])
            print ("best n for knn  = %s" % param_tuner.best_params_['n_neighbors'])

        elif (classifier == 'ada'):
            ada = AdaBoostClassifier() #Default base estimator is DT
            parameters = {'n_estimators':range(10,100,30)}
            param_tuner = grid_search.GridSearchCV(ada,parameters)
            param_tuner.fit(XTrain, yTrain)
            best_params[classifier] = (param_tuner.best_params_['n_estimators'])
            print ("best n_estimators for adaBoost  = %s" % param_tuner.best_params_['n_estimators'])

        elif (classifier == 'dt'):
            dt = dt = DecisionTreeClassifier()
            parameters = {'max_depth':range(10,50,20)}
            param_tuner = grid_search.GridSearchCV(dt,parameters)
            param_tuner.fit(XTrain, yTrain)
            best_params[classifier] = (param_tiner.best_params_['max_depth'])
            print ("best max_depth for dt  = %s" % param_tuner.best_params_['max_depth'])

    return best_params

def compare_classifiers(XTrain, yTrain, XTest, yTest, optimal_hp):

    #setup up vectors 
    fpr = []
    tpr = []
    auc = []
    f1 = []
    precision = []
    recall = []
    p_score = []
    r_score = []
    error = []
    times = []
    list_of_metrics = [fpr, tpr, auc, f1, precision, recall, p_score, r_score, error]

    #go through each classifier
    last_time = time.time()

    # NB - Gaussian
    (predictions, scores) = naive_bayes_g(XTrain, yTrain, XTest)
    new_time = time.time()
    times.append(new_time - last_time)
    last_time = new_time

    (fpr_i, tpr_i, auc_i, f1_i, precision_i, recall_i, p_score_i, r_score_i, error_i) = performance_metrics(yTest,scores,predictions)
    list_of_metrics = append_lists(list_of_metrics,
                        [fpr_i, tpr_i, auc_i, f1_i, precision_i,recall_i, p_score_i, r_score_i, error_i])
    print ('Finished with nb-gauss')

    # NB - Multinomial
    (predictions, scores) = naive_bayes_m(XTrain, yTrain, XTest)
    new_time = time.time()
    times.append(new_time - last_time)
    last_time = new_time

    (fpr_i, tpr_i, auc_i, f1_i, precision_i, recall_i, p_score_i, r_score_i, error_i) = performance_metrics(yTest,scores,predictions)
    list_of_metrics = append_lists(list_of_metrics,
                        [fpr_i, tpr_i, auc_i, f1_i, precision_i,recall_i, p_score_i, r_score_i, error_i])
    print ('Finished with nb-multi')

    # Decision Tree
    (predictions, scores) = decision_tree(XTrain, yTrain, XTest, optimal_hp['dt'])
    new_time = time.time()
    times.append(new_time - last_time)
    last_time = new_time

    (fpr_i, tpr_i, auc_i, f1_i, precision_i, recall_i, p_score_i, r_score_i, error_i) = performance_metrics(yTest,scores,predictions)
    list_of_metrics = append_lists(list_of_metrics,
                        [fpr_i, tpr_i, auc_i, f1_i, precision_i,recall_i, p_score_i, r_score_i, error_i])
    print ('Finished with dt')

    # SVM
    (predictions, scores) = support_vector(XTrain, yTrain, XTest)
    new_time = time.time()
    times.append(new_time - last_time)
    last_time = new_time

    (fpr_i, tpr_i, auc_i, f1_i, precision_i, recall_i, p_score_i, r_score_i, error_i) = performance_metrics(yTest,scores,predictions)
    list_of_metrics = append_lists(list_of_metrics,
                        [fpr_i, tpr_i, auc_i, f1_i, precision_i,recall_i, p_score_i, r_score_i, error_i])
    print ('Finished with svm')

    # adaBoost
    (predictions, scores) = ada_boost(XTrain, yTrain, XTest, optimal_hp['ada'])
    new_time = time.time()
    times.append(new_time - last_time)
    last_time = new_time

    (fpr_i, tpr_i, auc_i, f1_i, precision_i, recall_i, p_score_i, r_score_i, error_i) = performance_metrics(yTest,scores,predictions)
    list_of_metrics = append_lists(list_of_metrics,
                        [fpr_i, tpr_i, auc_i, f1_i, precision_i,recall_i, p_score_i, r_score_i, error_i])
    print ('Finished with adaBoost')

    # kNN 
    (predictions, scores) = nearest_neighbors(XTrain, yTrain, XTest, optimal_hp['knn'])
    new_time = time.time()
    times.append(new_time - last_time)
    last_time = new_time

    (fpr_i, tpr_i, auc_i, f1_i, precision_i, recall_i, p_score_i, r_score_i, error_i) = performance_metrics(yTest,scores,predictions)
    list_of_metrics = append_lists(list_of_metrics,
                        [fpr_i, tpr_i, auc_i, f1_i, precision_i,recall_i, p_score_i, r_score_i, error_i])
    print ('Finished with knn')

    return (list_of_metrics[0],list_of_metrics[1],list_of_metrics[2],
            list_of_metrics[3],list_of_metrics[4],list_of_metrics[5],
            list_of_metrics[6],list_of_metrics[7],list_of_metrics[8],
            times)

def append_lists(list_lists, list_vals):
    for i in range(len(list_lists)):
        list_lists[i].append(list_vals[i])

    return list_lists


def performance_metrics(labels, prob_scores, predictions):
    #putting labels into binary (NotSpam = 0)
    encoder = LabelEncoder()
    encoder.fit(["NotSpam","Spam"])
    labels_binary = encoder.transform(labels)
    predictions_binary = encoder.transform(predictions)

    #roc_curve returns false-positive and true-positive rates
    (fpr, tpr, _) = roc_curve(labels_binary, prob_scores[:,1])

    #AUC (area under ROC curve)
    auc_temp = roc_auc_score(labels_binary,prob_scores[:,1])

    #f1 (combination of precision and recall)
    f1 = f1_score(labels, predictions, pos_label='Spam')

    #precision/recall estimates
    (precision, recall, _) = precision_recall_curve(labels_binary, prob_scores[:,1])

    #precision/recall scores
    p_score = precision_score(labels, predictions, pos_label='Spam')
    r_score = recall_score(labels, predictions, pos_label='Spam')

    #error
    error = find_error(predictions, labels)

    return (fpr, tpr, auc_temp, f1, precision, recall, p_score, r_score, error)


def find_error(predictions, truth):
    predictions = np.array(predictions)
    truth = np.array(truth)
    return (100.0 * np.sum(predictions != truth)) / len(predictions)











